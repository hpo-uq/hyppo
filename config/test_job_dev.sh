#!/bin/bash
#SBATCH --constraint haswell
#SBATCH --account m1248
#SBATCH --qos debug
#SBATCH --job-name temp-cpu
#SBATCH --time 4
#SBATCH --ntasks 32
#SBATCH --cpus-per-task 1
#SBATCH --output %x-%j.out
#SBATCH --error %x-%j.err
module load parallel
module load pytorch/1.7.1
export PYTHONPATH=$PYTHONPATH:$HOME/hyppo
python $HOME/hyppo/bin/hyppo sampling test_example.yaml
parallel --delay .2 -j 4 " srun --exclusive --nodes 1 --ntasks 8 --cpus-per-task 1 --mem=20GB --gres=craynetwork:0 python $HOME/hyppo/bin/hyppo evaluation test_example.yaml && echo step {1}" ::: {0..3}
