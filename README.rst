HPO-UQ
======

.. contents::

Documentation
-------------

- `master <https://hpo-uq.gitlab.io/hyppo/>`_

Why HPO-UQ?
-----------

While there is already a plethora of hyperparameter optimization tools publicly available for machine learning applications, none of them take into account an essential aspect of such models which is that neural network models are built using a stochastic solver approach. The HPO-UQ software was developed with the goal of implementing uncertainty quantification on the models' parameters.

Copyright Notice
----------------

HYPPO Copyright (c) 2021, The Regents of the University of California,
through Lawrence Berkeley National Laboratory (subject to receipt of
any required approvals from the U.S. Dept. of Energy). All rights reserved.

If you have questions about your rights to use or distribute this software,
please contact Berkeley Lab's Intellectual Property Office at
IPO@lbl.gov.

NOTICE.  This Software was developed under funding from the U.S. Department
of Energy and the U.S. Government consequently retains certain rights.  As
such, the U.S. Government has been granted for itself and others acting on
its behalf a paid-up, nonexclusive, irrevocable, worldwide license in the
Software to reproduce, distribute copies to the public, prepare derivative 
works, and perform publicly and display publicly, and to permit others to do so.

