#!/bin/bash
pip install PyYAML==5.1
git clone https://gitlab.com/hpo-uq/hyppo
pip install -r hyppo/requirements.txt
ln -s /content/hyppo/hyppo $(python -c "import pip; print(pip.__path__[0].rstrip('/pip'))")/
