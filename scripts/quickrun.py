#!/usr/bin/env python

# Internals
import argparse

# Externals
import hyppo

parser = argparse.ArgumentParser('quickrun.py')
parser.add_argument('operation', help='Operation to execute')
parser.add_argument('input_data', nargs='*', help='Path to input data')
parser.add_argument('-c','--config', help='Input configuration file')
parser.add_argument('-m','--mpi-cores', type=int, help='Number of MPI cores')
parser.add_argument('-o','--output-name', help='Output filename')
parser.add_argument('-l','--limits', nargs=2, type=float, help='Output filename')
parser.add_argument('-t','--tensorboard',action='store_true',help='Use tensorboard log files.')
parser.add_argument('-v','--verbose', help='Do verbose')
args = parser.parse_args()

try:
    getattr(hyppo,args.operation)(**vars(args))
except AttributeError:
    print("Method '%s' not found in HPO-UQ."%args.operation)
    quit()
