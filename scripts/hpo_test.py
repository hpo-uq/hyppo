#!/usr/bin/env python

import torch
import hyppo

# Check Models
hyperprms = {'layers':2,'nodes':[10,10],'activation':['relu','relu'],'dropout':[0.1,0.1]}
in_data, out_data = torch.rand(50,7), torch.rand(5,1)
in_shape, out_shape = in_data.shape, out_data.shape
hyppo.dnnmodels.mlp_pt.PytorchMLP(in_shape,out_shape,hyperprms)

