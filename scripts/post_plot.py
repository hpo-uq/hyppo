#!/usr/bin/env python
import sys
import yaml
import glob
import argparse
import importlib
from hyppo.dnnmodels.pytorch import evaluate
from hyppo.dnnmodels.pytorch.utils import uq_quantifer, load_model
from hyppo.datasets import get_data, get_loader
from hyppo.hyperparams import get_hyperprms

def parse_args():
    """
    Parse command line arguments.
    """
    parser = argparse.ArgumentParser('hpo.py')
    add_arg = parser.add_argument
    add_arg('config_file', help='Input configuration file')
    add_arg('model', help='Path to trained model')
    return parser.parse_args()

def main():
    args = parse_args()
    
    # Load configuration file
    with open(args.config_file) as f:
        config = yaml.load(f, Loader=yaml.FullLoader)
        
    # Get samples from model file
    samples = args.model.split('-')[1].split('_')

    # Create hyperparameter set
    prms = config['prms']
    x_sc = [int(samples[n])*prms['mult'][n] for n in range(len(prms['mult']))]
    x_sc = {name:value for name,value in zip(prms['names'],x_sc)}
    if 'default' in prms.keys() and type(prms['default'])==dict:
        x_sc = {**x_sc,**prms['default']}
    hyperprms = get_hyperprms(x_sc, **config['model'])
    criterion = hyperprms['loss'](**hyperprms['loss_args'])
        
    # Prepare dataset
    dicts = {**config['model'],**config['data']}
    data = get_data(**dicts)
    config['data'] = {**config['data'], **data}
    data = get_loader(data, **hyperprms, **config['model'], **config['dist'])

    # Architecture initialization
    module = importlib.import_module('.' + config['model']['dl_type'].lower(), 'hyppo.dnnmodels.pytorch')
    net = module.get_model(data=data['train'], prms=hyperprms).to('cpu')
    models = []
    for model in sorted(glob.glob(args.model[:-10]+'*.pth.tar')):
        models.append(load_model(model,net))
    loss = evaluate('cpu', data['test'], models[0], criterion, verbose=True, **config['model'])
    
    # Execture UQ quantifier
    data_variance = config['uq'].get('data_noise',0.0)
    weights = config['uq'].get('uq_weights', [0.5,0.5])
    uq_quantifer(data, models, hyperprms, criterion, data_variance, weights, plot_type='time_series', **config['model'])
    
if __name__ == '__main__':
    main()

