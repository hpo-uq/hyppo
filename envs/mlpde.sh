#!/bin/bash
module load pytorch/1.9.0-gpu
pip install torchvision
module load cudatoolkit/10.2.89_3.28-7.0.1.1_2.1__g88d3d59
git clone https://github.com/NVIDIA/apex
cd apex
pip install -v --no-cache-dir --global-option="--cpp_ext" --global-option="--cuda_ext" ./
pip install ruamel.yaml
pip install wandb
