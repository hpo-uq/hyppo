#!/bin/bash
conda create -n hep_cpu python=3.7 -y
eval "$(conda shell.bash hook)"
pip install tensorflow==2.6.0
pip install -r ~/hyppo/requirements.txt
pip install tqdm dm-sonnet graph-nets
