#!/bin/bash
conda create -n tomopy python=3.7 -y
eval "$(conda shell.bash hook)"
conda activate tomopy
conda install cudatoolkit==11.0.221 -y
pip install tensorflow-gpu==2.4.0
conda install -c conda-forge cudnn==8.2.1.32 -y
pip install -r ~/hyppo/requirements.txt
conda install -c conda-forge tomopy==1.10.1 -y
pip install scikit-image==0.18.2
pip install horovod==0.21.0
pip install cloudpickle==1.4.1
pip install tensorflow_probability==0.11.0